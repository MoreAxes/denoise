#include <algorithm>
#include <chrono>
#include <vector>
#include <memory>
#include <mutex>
#include <utility>
#include <iostream>
#include <unordered_map>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include "nanoflann.hpp"

using namespace cv;
using namespace nanoflann;

using namespace std::chrono;
using millis = std::chrono::milliseconds;

const int KNN_QUERY_SIZE = 8;
float PATCH_ACCEPTANCE_THRESHOLD = 0.02;
constexpr size_t FEATURE_DIM = 12;
static bool PRINT_PROGRESS = false;

struct RaiiTimer {
  RaiiTimer(const char* name) :
    name_(name),
    start_(high_resolution_clock::now())
  {
    if (name_)
      std::cout << name << "... ";
  }

  ~RaiiTimer() {
    if (name_)
      std::cout << "done (" << ((float) duration_cast<millis>(high_resolution_clock::now() - start_).count()) / 1000.0 << "s)\n";
  }

  static std::unique_ptr<RaiiTimer> start(const char* name) {
    return std::make_unique<RaiiTimer>(name);
  }

  time_point<high_resolution_clock> start_;
  const char* name_;
};

#define TIMER_INIT() auto t_macro_internal = RaiiTimer::start(nullptr);
#define TIMER_START(name) t_macro_internal = RaiiTimer::start(name);
#define TIMER_END(name) t_macro_internal = nullptr;

struct Feature {
  char x[FEATURE_DIM];

  char operator [] (const size_t idx) const {
    return x[idx];
  }
};

struct FeatureCloud
{
  typedef char coord_t;
  std::vector<Feature> pts;
  std::vector<Point2i> positions;

  FeatureCloud(const std::vector<Feature>& pts, const std::vector<Point2i>& positions) : pts(pts), positions(positions) {}

  Feature& operator [] (const size_t idx) {
    return pts[idx];
  }
};

struct FeatureCloudAdaptor
{
  typedef char coord_t;

  const FeatureCloud& obj;

  FeatureCloudAdaptor(const FeatureCloud& obj_) : obj(obj_) { }
  inline const FeatureCloud& derived() const { return obj; }
  inline size_t kdtree_get_point_count() const { return derived().pts.size(); }

  // Returns the dim'th component of the idx'th point in the class
  inline coord_t kdtree_get_pt(const size_t idx, int dim) const {
    return derived().pts[idx][dim];
  }

  inline bool kdtree_is_allowed(const size_t idx) const {
    return true;
  }

  template <class BBOX>
  bool kdtree_get_bbox(BBOX& /*bb*/) const { return false; }
};

template<class T, class DataSource, typename _DistanceType = float>
struct L2_Filtered_Adaptor {
  typedef T ElementType;
  typedef _DistanceType DistanceType;

  const DataSource &data_source;

  L2_Filtered_Adaptor(const DataSource &_data_source) : data_source(_data_source) { }

  inline DistanceType evalMetric(const T* a, const size_t b_idx, size_t size) const {
    DistanceType result = DistanceType();
    for (size_t i = 0; i < size; ++i) {
      const DistanceType diff = (float) (a[i] - data_source.kdtree_get_pt(b_idx, i));
      result += diff * diff;
    }
    return result;
  }

  inline bool isAllowed(const size_t b_idx) const {
    return data_source.kdtree_is_allowed(b_idx);
  }

  template <typename U, typename V>
  inline DistanceType accum_dist(const U a, const V b, int ) const
  {
    return (a - b) * (a - b);
  }
};

typedef KDTreeSingleIndexAdaptor<
    L2_Filtered_Adaptor<char, FeatureCloudAdaptor>,
    FeatureCloudAdaptor,
    FEATURE_DIM
    > dct_kdtree_t;

Feature single_feature(const std::vector<Mat>& channels, Rect roi) {
  Feature feat;
  for (int ch = 0; ch < channels.size(); ++ch) {
    Mat patch = Mat(channels[ch], roi);
    Mat dest;
    resize(patch, dest, Size(2, 2), 0, 0, INTER_AREA);
    int k = 0;
    for (auto f = dest.begin<float>(); f != dest.end<float>(); f++) {
      feat.x[ch * 4 + k] = (*f * 64);
      k++;
    }
  }
  return feat;
}

FeatureCloud compute_features(Mat image, int window_size) {
  std::vector<Mat> channels;
  split(image, channels);
  int i = 0;
  std::vector<Feature> dct_features;
  std::vector<Point2i> positions;
  int height = image.size[0] - window_size;
  int width = image.size[1] - window_size;
  dct_features.reserve(height * width);
  positions.reserve(height * width);
  for (int y = 0; y <= height; ++y) {
    for (int x = 0; x <= width; ++x) {
      Point2i position(x, y);
      dct_features.push_back(single_feature(channels, Rect(x, y, window_size, window_size)));
      positions.push_back(position);
    }
  }

  return FeatureCloud(dct_features, positions);
}

std::vector<size_t> select_feature_indices(dct_kdtree_t& index, const Feature* query, int k) {
  std::vector<size_t> ret_index(k);
  std::vector<float> out_dist_sqr(k);
  size_t actual = index.knnSearch((char*) query, k, &ret_index[0], &out_dist_sqr[0]);
  ret_index.resize(actual);
  out_dist_sqr.resize(actual);

  return ret_index;
}

std::vector<Point2i> indices_to_coords(dct_kdtree_t& index, const std::vector<size_t>& indices) {
  std::vector<Point2i> result(indices.size());
  for (int it = 0; it < indices.size(); ++it) {
    result[it] = index.dataset.obj.positions[indices[it]];
  }

  return result;
}

struct Wrap {
  Mat3f& lab_image;
  dct_kdtree_t& index;
  FeatureCloudAdaptor& adaptor;
  int window_size;
  char* out_image_path;
  std::vector<std::vector<size_t>>& contrib_index;
  std::vector<std::vector<float>>& diff_index;
};

void denoise(Mat3f lab_image, int window_size, const std::vector<std::vector<size_t>>& contrib_index, const std::vector<std::vector<float>>& diff_index, dct_kdtree_t& index, FeatureCloudAdaptor& adaptor, const char* out_image_path) {
  TIMER_INIT();
  TIMER_START("denoising");
  std::mutex out_mutex;
  Mat3f denoised_image = Mat3f(lab_image.size(), Vec3f(0, 0, 0));
  Mat3f patch_weights = Mat3f(lab_image.size(), Vec3f(0, 0, 0));
  Mat1f patch_contribution = Mat1f(lab_image.size(), 0);
  size_t patches_denoised = 0;
  #pragma omp parallel for
  for (size_t it = 0; it < adaptor.obj.pts.size(); ++it) {
    Point2i this_pt = adaptor.obj.positions[it];
    int x = this_pt.x;
    int y = this_pt.y;
    Rect original_roi = Rect(x, y, window_size, window_size);
    Mat3f original_patch = lab_image(original_roi);
    Mat3f patch = original_patch.clone() / (window_size * window_size);
    std::vector<Point2i> points = indices_to_coords(index, contrib_index[it]);
    if (!points.empty()) {
      float patches = 1.0 / (window_size * window_size);
      for (size_t jt = 0; jt < points.size(); ++jt) {
        Rect roi(points[jt], Size(window_size, window_size));
        Mat new_patch = lab_image(roi);
        if (diff_index[it][jt] < PATCH_ACCEPTANCE_THRESHOLD) {
          patch += new_patch;
          patches++;
        }
      }
      patch /= patches;
      {
        std::lock_guard<std::mutex> lock(out_mutex);
        denoised_image(Rect(x, y, window_size, window_size)) += patch;
        patch_weights(Rect(x, y, window_size, window_size)) += Vec3f(1, 1, 1);
        patch_contribution(Rect(x, y, window_size, window_size)) += Vec3f(patches, patches, patches);
        patches_denoised++;
        if (PRINT_PROGRESS && patches_denoised % (lab_image.size[1] - window_size) == 0) {
          std::cout << 100 * ((float) patches_denoised) / adaptor.obj.pts.size() << "%\n";
        }
      }
    }
  }
  TIMER_END();
  denoised_image = denoised_image.mul(1.0 / patch_weights);
  denoised_image = denoised_image.mul(Mat3f(denoised_image.size(), Vec3f(100, 255, 255)));
  denoised_image -= Vec3f(0, 127, 127);
  cvtColor(denoised_image, denoised_image, COLOR_Lab2BGR);
  imshow("Denoised Image", denoised_image);
  imshow("Contributed Patches Image", patch_contribution / (window_size * window_size * KNN_QUERY_SIZE));
  Mat out_image;
  denoised_image.convertTo(out_image, CV_8U, 255.0);
  imwrite(out_image_path, out_image);
}

void on_mouse(int event, int x, int y, int flags, void* userdata) {
  Wrap* w = (Wrap*) userdata;

  if (event == EVENT_LBUTTONDOWN || event == EVENT_RBUTTONDOWN) {
    size_t it = std::find_if(w->adaptor.obj.positions.begin(),
                             w->adaptor.obj.positions.end(),
                             [&] (const Point2i& pt) { return pt.x == x && pt.y == y; })
                - w->adaptor.obj.positions.begin();
    Point2i this_pt = w->adaptor.obj.positions[it];
    Rect original_roi = Rect(x, y, w->window_size, w->window_size);
    Mat3f original_patch = w->lab_image(original_roi);
    std::vector<Point2i> points = indices_to_coords(w->index, w->contrib_index[it]);

    std::vector<Rect> committed_rois;
    if (!points.empty()) {
      int patches = 1;
      for (auto pt : points) {
        Rect roi(pt, Size(w->window_size, w->window_size));
        Mat new_patch = w->lab_image(roi);
        committed_rois.push_back(roi);
      }
    }
    std::cout << committed_rois.size() << " patches\n";
    Mat image = w->lab_image.clone();
    for (auto& r : committed_rois) {
      rectangle(image, r, Scalar(1.0, 0.0, 0.0));
    }
    image = image.mul(Mat3f(image.size(), Vec3f(100, 255, 255)));
    image -= Vec3f(0, 127, 127);
    cvtColor(image, image, COLOR_Lab2BGR);
    imshow("Display Image", image);

    if (event == EVENT_RBUTTONDOWN) {
      Mat3f patch = Mat3f(original_patch.size());
      for (auto& r : committed_rois) {
        patch += w->lab_image(r);
      }

      patch /= committed_rois.size();
      resize(patch.clone(), patch, Size(), 8, 8);
      Mat3f comparison(patch.size[1], patch.size[0] * 2, Vec3f(0, 0, 0));
      resize(original_patch, comparison(Rect(Point(0, 0), patch.size())), Size(), 8, 8);
      patch.copyTo(comparison(Rect(Point(w->window_size*8, 0), patch.size())));
      comparison = comparison.mul(Mat3f(comparison.size(), Vec3f(100, 255, 255)));
      comparison -= Vec3f(0, 127, 127);
      cvtColor(comparison, comparison, COLOR_Lab2BGR);
      imshow("comparison", comparison);
    }
  }

  if (event == EVENT_MOUSEWHEEL) {
    PATCH_ACCEPTANCE_THRESHOLD += (flags > 0 ? 0.001 : -0.001);
    std::cout << "patch acceptance threshold: " << PATCH_ACCEPTANCE_THRESHOLD << "\n";
  }

  if (event == EVENT_MBUTTONDOWN) {
    denoise(w->lab_image, w->window_size, w->contrib_index, w->diff_index, w->index, w->adaptor, w->out_image_path);
  }
}

extern "C" void* den_load_image(char* path, size_t* out_width, size_t* out_height);
extern "C" size_t den_train_and_extract_features(void** images, size_t images_count, size_t aperture, size_t features, float* out_ptr);
extern "C" size_t den_get_feature_vector_count(void* image, size_t aperture);

int main(int argc, char** argv)
{
  TIMER_INIT();
  PRINT_PROGRESS = getenv("PRINT_PROGRESS");

  if (argc < 3) {
    printf("usage: denoise <image_path> <out_path>\n");
    return -1;
  }
  Mat3f image = imread(argv[1], CV_LOAD_IMAGE_COLOR);
  image /= 255;
  Mat3f lab_image;
  cvtColor(image, lab_image, COLOR_BGR2Lab);
  lab_image += Vec3f(0, 127, 127);
  lab_image = lab_image.mul(Mat3f(lab_image.size(), Vec3f(1.0 / 100, 1.0 / 255, 1.0 / 255)));

  if (argc > 3) {
    Mat3f noise(lab_image.size(), Vec3f(0, 0, 0));
    randn(noise, 0, 0.1);
    noise = noise.mul(Mat3f(noise.size(), Vec3f(1, 0, 0)));
    lab_image += noise;

    Mat lab_image_2 = lab_image.clone();
    lab_image_2 = lab_image_2.mul(Mat3f(lab_image_2.size(), Vec3f(100, 255, 255)));
    lab_image_2 -= Vec3f(0, 127, 127);
    cvtColor(lab_image_2, lab_image_2, COLOR_Lab2BGR);
    Mat noisy_img;
    lab_image_2.convertTo(noisy_img, CV_8U, 255.0);
    imshow("Display Image", noisy_img);
    imwrite(argv[3], noisy_img);
  } else {
    imshow("Display Image", image);
  }

  std::cout << image.size() << "\n";

  int window_size = 8;
  TIMER_START("computing features");
  void* ext_image;
  size_t width;
  size_t height;
  ext_image = den_load_image(argv[1], &width, &height);
  size_t ext_feature_vector_count = den_get_feature_vector_count(ext_image, window_size / 2);
  std::cout << "will produce " << ext_feature_vector_count << " fvs per image\n";
  // std::vector<Feature> features;
  // features.reserve(ext_feature_vector_count * lab_images.size());
  Feature* fvs = new Feature[ext_feature_vector_count];
  std::cout << "out ptr: " << fvs << "\n";
  size_t ext_features_written = den_train_and_extract_features(&ext_image, 1, window_size / 2, FEATURE_DIM, (float*) fvs);
  std::cout << "got " << ext_features_written << " fvs\n";
  std::vector<Feature> features(fvs, fvs + ext_feature_vector_count);
  delete[] fvs;
  std::vector<Point2i> positions;
  for (size_t img = 0; img < 1; ++img) {
    for (size_t y = 0; y <= height - window_size; ++y) {
      for (size_t x = 0; x <= width - window_size; ++x) {
        positions.push_back(Point2i(x, y));
      }
    }
  }
  FeatureCloud feat_cloud(features, positions);
  // FeatureCloud feat_cloud = compute_features(lab_image, window_size);
  FeatureCloudAdaptor adaptor(feat_cloud);
  TIMER_END();

  TIMER_START("constructing kdtree");
  dct_kdtree_t index(FEATURE_DIM, adaptor, KDTreeSingleIndexAdaptorParams(10));
  index.buildIndex();
  TIMER_END();

  std::mutex out_mutex;
  size_t features_linked = 0;
  std::vector<std::pair<size_t, std::vector<size_t>>> contrib_index;
  std::vector<std::pair<size_t, std::vector<float>>> diff_index;

  #pragma omp declare reduction(\
      merge_vectors_size_t:\
      std::vector<std::pair<size_t, std::vector<size_t>>>:\
      omp_out.insert(omp_out.end(), omp_in.begin(), omp_in.end()))\
      initializer(omp_priv = {})
  #pragma omp declare reduction(\
      merge_vectors_float:\
      std::vector<std::pair<size_t, std::vector<float>>>:\
      omp_out.insert(omp_out.end(), omp_in.begin(), omp_in.end()))\
      initializer(omp_priv = {})

  TIMER_START("constructing feature index");
  #pragma omp parallel for reduction(merge_vectors_size_t:contrib_index) reduction(merge_vectors_float:diff_index)
  for (size_t it = 0; it < adaptor.obj.pts.size(); ++it) {
    std::vector<size_t> point_indices = select_feature_indices(index, &adaptor.obj.pts[it], KNN_QUERY_SIZE);
    std::vector<float> diffs;
    Point2i this_pt = adaptor.obj.positions[it];
    int x = this_pt.x;
    int y = this_pt.y;

    Rect original_roi = Rect(x, y, window_size, window_size);
    Mat3f original_patch = lab_image(original_roi);
    std::vector<Point2i> points = indices_to_coords(index, point_indices);
    if (!points.empty()) {
      for (auto pt : points) {
        Rect roi(pt, Size(window_size, window_size));
        Mat new_patch = lab_image(roi);
        diffs.push_back(norm(new_patch - original_patch) / (window_size * window_size));
      }
    }

    contrib_index.emplace_back(std::make_pair(it, std::move(point_indices)));
    diff_index.emplace_back(std::make_pair(it, std::move(diffs)));
    if (PRINT_PROGRESS) {
      std::lock_guard<std::mutex> lock(out_mutex);
      features_linked++;
      if (PRINT_PROGRESS && features_linked % (lab_image.size[1] - window_size) == 0) {
        std::cout << 100 * ((float) features_linked) / adaptor.obj.pts.size() << "%\n";
      }
    }
  }
  TIMER_END();

  TIMER_START("ordering indices");
  std::vector<std::vector<size_t>> contrib_index_ordered(contrib_index.size(), std::vector<size_t>());
  std::vector<std::vector<float>> diff_index_ordered(diff_index.size(), std::vector<float>());
  for (auto&& ci : contrib_index) {
    contrib_index_ordered[ci.first] = std::move(ci.second);
  }
  for (auto&& di : diff_index) {
    diff_index_ordered[di.first] = std::move(di.second);
  }
  TIMER_END();

  Wrap w{lab_image, index, adaptor, window_size, argv[2], contrib_index_ordered, diff_index_ordered};
  setMouseCallback("Display Image", on_mouse, &w);

  while (true)
    waitKey();

  return 0;
}
