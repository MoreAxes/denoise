#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

uint64_t rdtsc(){
  unsigned int lo,hi;
  __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
  return ((uint64_t)hi << 32) | lo;
}

int main(int argc, char const *argv[])
{
  using namespace cv;

  std::vector<Mat3f> lab_images;
  std::vector<std::string> out_paths;

  for (int at = 1; at < argc; at += 2) {
    std::cout << argv[at] << '\n';
    Mat3f image = imread(argv[at], CV_LOAD_IMAGE_COLOR);
    image /= 255;
    Mat3f lab_image;
    cvtColor(image, lab_image, COLOR_BGR2Lab);
    lab_image += Vec3f(0, 127, 127);
    lab_image = lab_image.mul(Mat3f(lab_image.size(), Vec3f(1.0 / 100, 1.0 / 255, 1.0 / 255)));
    lab_images.push_back(lab_image);
    out_paths.emplace_back(argv[at+1]);
  }

  #pragma omp parallel for
  for (int it = 0; it < lab_images.size(); ++it) {
    RNG rng(rdtsc());
    Mat1f noise_l(lab_images[it].size(), 0.0);
    Mat1f noise_a(lab_images[it].size(), 0.0);
    Mat1f noise_b(lab_images[it].size(), 0.0);
    rng.fill(noise_l, RNG::NORMAL, 0, 0.1);
    std::vector<Mat1f> channels = {noise_l, noise_a, noise_b};
    Mat3f noise;
    merge(channels, noise);
    // noise = noise.mul(Mat3f(noise.size(), Vec3f(1, 0, 0)));
    lab_images[it] += noise;

    lab_images[it] = lab_images[it].mul(Mat3f(lab_images[it].size(), Vec3f(100, 255, 255)));
    lab_images[it] -= Vec3f(0, 127, 127);
    cvtColor(lab_images[it], lab_images[it], COLOR_Lab2BGR);
    Mat noisy_img;
    lab_images[it].convertTo(noisy_img, CV_8U, 255.0);
    std::cout << out_paths[it] << "\n";
    imwrite(out_paths[it], noisy_img);
  }

  return 0;
}