#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

int main(int argc, char const *argv[])
{
  using namespace cv;

  std::vector<Mat3f> lab_images;
  std::vector<std::string> out_paths;

  Mat3f image1 = imread(argv[1], CV_LOAD_IMAGE_COLOR);
  Mat3f image2 = imread(argv[2], CV_LOAD_IMAGE_COLOR);
  Mat3f lab_image1;
  Mat3f lab_image2;
  cvtColor(image1 / 255.0, lab_image1, COLOR_BGR2Lab);
  cvtColor(image2 / 255.0, lab_image2, COLOR_BGR2Lab);

  Mat3f diff = (lab_image2 - lab_image1) * 5 + Vec3f(50, 0.5, 0.5);
  Mat3f diff_rgb;
  cvtColor(diff, diff_rgb, COLOR_Lab2BGR);
  Mat out_img;
  diff_rgb.convertTo(out_img, CV_8U, 255.0);

  imwrite(argv[3], out_img);

  Mat image1_u8;
  Mat image2_u8;
  image1.convertTo(image1_u8, CV_8U, 255.0);
  image2.convertTo(image2_u8, CV_8U, 255.0);
  std::cout << "PSNR " << PSNR(image1_u8, image2_u8) << "\n";

  return 0;
}