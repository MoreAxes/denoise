extern crate clap;
extern crate fern;
extern crate fingers as nn;
extern crate fnv;
extern crate image;
#[macro_use]
extern crate itertools;
#[macro_use]
extern crate log;
extern crate palette;
extern crate rand;
extern crate rayon;
extern crate serde_json as sj;
#[macro_use]
extern crate structopt_derive;
extern crate structopt;
extern crate strum;
#[macro_use]
extern crate strum_macros;
extern crate time;

use palette::{FromColor, Lab};
use palette::pixel::Srgb;
use rayon::prelude::*;

use std::str::FromStr;

mod common;
mod filters;

use common::*;

#[derive(StructOpt, Debug)]
pub struct Opt {
  #[structopt(long = "log-level")]
  log_level: Option<String>,
  #[structopt]
  input: Vec<String>,
  #[structopt(long = "output", short = "o", default_value = "denoised.png")]
  output: String,
  #[structopt(long = "op")]
  operation: filters::Operation,
  #[structopt(long = "output-diff", short = "d")]
  output_diff: Option<String>,
  #[structopt(long = "aperture", short = "a", default_value = "0")]
  aperture: usize,
  #[structopt(long = "rank-function", short = "r", default_value = "euclidean")]
  rank_function: filters::RankFunction,
  #[structopt(long = "dimension-scale", short = "s", default_value = "1,1,1")]
  dimension_scale: filters::DimScale,
  #[structopt(long = "optimize-lowest-ranking-sample", short = "p")]
  optimize: bool,
  #[structopt(long = "mean-of-lowest-ranking", short = "m", default_value = "0")]
  mean_of_lowest: usize,
  #[structopt(long = "subrect")]
  subrect: Option<SubRect>,
}

#[derive(Debug, Clone, Copy)]
pub struct SubRect(u32, u32, u32, u32);

impl FromStr for SubRect {
  type Err = clap::Error;

  fn from_str(s: &str) -> clap::Result<Self> {
    let mut parts = s.split(",");
    let x = parts.next().unwrap_or("0").parse().unwrap_or(0);
    let y = parts.next().unwrap_or("0").parse().unwrap_or(0);
    let w = parts.next().unwrap_or("1").parse().unwrap_or(1);
    let h = parts.next().unwrap_or("1").parse().unwrap_or(1);
    Ok(SubRect(x, y, w, h))
  }
}

fn setup_logging(log_spec_str: &str) -> Result<(), log::SetLoggerError> {
  let log_spec: Vec<(Option<String>, log::LogLevelFilter)> = log_spec_str
    .split(",")
    .filter_map(|sp| {
      let parts: Vec<_> = sp.split("=").collect();
      match parts.len() {
        1 => parts[0].parse().ok().map(|lvl| (None, lvl)),
        2 => {
          parts[1].parse().ok().map(
            |lvl| (Some(parts[0].into()), lvl),
          )
        }
        _ => {
          eprintln!("malformed log spec: '{}'", sp);
          None
        }
      }
    })
    .collect();
  let mut dispatch = fern::Dispatch::new()
    .format(|out, message, record| {
      out.finish(format_args!(
        "[{} {}] {}",
        record.level(),
        record.location().module_path(),
        message
      ))
    })
    .chain(std::io::stderr())
    .level(log::LogLevelFilter::Error);

  for (maybe_path, level) in log_spec {
    match maybe_path {
      None => dispatch = dispatch.level(level),
      Some(path) => dispatch = dispatch.level_for(path, level),
    }
  }

  dispatch.apply()
}

fn main() {
  use structopt::StructOpt;
  let opt = Opt::from_args();
  let opt_ref = &opt;
  if let Err(err) = setup_logging(opt.log_level.as_ref().map(|s| s.as_str()).unwrap_or("")) {
    eprintln!("logging setup failed: {}", err);
  }

  let input_images: Vec<_> = opt
    .input
    .iter()
    .map(|path| {
      let mut img = image::open(path)
          .map(|result_img| result_img.to_rgb())
          .expect(&format!("could not open {} or it failed to parse", path));
      if let &Some(sr) = &opt.subrect {
        use image::GenericImage;
        img = img.sub_image(sr.0, sr.1, sr.2, sr.3).to_image();
      }
      LabImage::from_srgb(&img)
    })
    .collect();

  if !input_images[1..].iter().map(|img| img.dimensions()).all(
    |dim| {
      dim == input_images[0].dimensions()
    },
  )
  {
    error!("Cannot proceed - images are not the same size.");
  }

  let input_images_ref = &input_images[..];

  let aperture = -(opt.aperture as isize)..(opt.aperture as isize + 1);
  let kernel = iproduct!(aperture.clone(), aperture).collect::<Vec<_>>();
  let kernel_ref = &kernel[..];
  let (out_width, out_height) = (
    input_images[0].width - opt.aperture * 2,
    input_images[0].height - opt.aperture * 2,
  );
  // let mut diff = image::ImageBuffer::<image::Rgb<u8>, Vec<u8>>::new(out_width as u32, out_height as u32);
  let result = LabImage::from_raw(
    out_width,
    out_height,
    (0..out_height)
      .into_par_iter()
      .map(move |y| {
        (0..out_width)
          .map(move |x| {
            let labs = (0..input_images_ref.len())
              .map(move |it| {
                kernel_ref.iter().map(move |&(dx, dy)| {
                  input_images_ref[it].get_pixel(
                    x + (opt_ref.aperture as isize + dx) as usize,
                    y + (opt_ref.aperture as isize + dy) as usize,
                  )
                })
              })
              .flat_map(|piece| piece)
              .cloned()
              .collect();

            filters::apply_filter(labs, input_images_ref.len(), opt_ref)
            // let diff_lab = new_lab - *input_images_ref[0].get_pixel(x + opt_ref.aperture, y + opt_ref.aperture);
            // diff_img_ref.add_pixel(x, y, &diff_lab);
          })
          .collect::<Vec<_>>()
      })
      .fold(|| Vec::with_capacity(out_width), |mut acc, row| {
        acc.extend(row);
        acc
      })
      .flat_map(|chunk| chunk)
      .collect::<Vec<_>>(),
  );

  let result_ref = &result;
  let diff_img = image::ImageBuffer::<image::Rgb<u8>, Vec<u8>>::from_raw(
    out_width as u32,
    out_height as u32,
    (0..out_height)
      .into_par_iter()
      .map(move |y| {
        (0..out_width)
          .flat_map(move |x| {
            let in_lab = input_images_ref[0].get_pixel(x + opt_ref.aperture, y + opt_ref.aperture);
            let out_lab = result_ref.get_pixel(x, y);
            let diff_lab = *out_lab - *in_lab;
            let new_srgb = Srgb::from_linear(diff_lab);
            vec![
              (new_srgb.red) as u8,
              (new_srgb.green) as u8,
              (new_srgb.blue) as u8,
            ]
          })
          .collect::<Vec<_>>()
      })
      .fold(|| Vec::with_capacity(out_width), |mut acc, row| {
        acc.extend(row);
        acc
      })
      .flat_map(|chunk| chunk)
      .collect::<Vec<_>>(),
  ).expect("failed to construct image from raw buffer");

  let result_srgb = image::ImageBuffer::<image::Rgb<u8>, Vec<u8>>::from_raw(
    out_width as u32,
    out_height as u32,
    (0..out_height)
      .into_par_iter()
      .map(move |y| {
        (0..out_width)
          .flat_map(move |x| {
            let out_lab = result_ref.get_pixel(x, y);
            let new_srgb = Srgb::from_linear(*out_lab);
            vec![
              (new_srgb.red * 255.0) as u8,
              (new_srgb.green * 255.0) as u8,
              (new_srgb.blue * 255.0) as u8,
            ]
          })
          .collect::<Vec<_>>()
      })
      .fold(|| Vec::with_capacity(out_width), |mut acc, row| {
        acc.extend(row);
        acc
      })
      .flat_map(|chunk| chunk)
      .collect::<Vec<_>>(),
  ).expect("failed to construct image from raw buffer");

  result_srgb.save(&opt.output).expect(&format!(
    "failed to write to {}",
    opt.output
  ));
  if let Some(ref path) = opt.output_diff {
    diff_img.save(path).expect(&format!(
      "failed to write to {}",
      opt.output
    ));
  } else {
    diff_img.save(format!("diff/{}", opt.output)).expect(&format!(
      "failed to write to {}",
      opt.output
    ));
  }
}
