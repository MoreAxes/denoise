use ::*;

use palette::{FromColor, Lab};
use palette::pixel::Srgb;
use rand::{SeedableRng};
use rand::distributions::IndependentSample;
use rayon::prelude::*;

pub struct LabImage {
  pub width: usize,
  pub height: usize,
  pub data: Vec<Lab>,
}

impl LabImage {
  pub fn from_srgb(srgb_img: &image::ImageBuffer<image::Rgb<u8>, Vec<u8>>) -> Self {
    Self {
      width: srgb_img.width() as usize,
      height: srgb_img.height() as usize,
      data: srgb_img
        .get(..)
        .unwrap()
        .par_chunks(3)
        .map(|srgb| {
          Srgb::new_u8(srgb[0], srgb[1], srgb[2]).to_linear()
        })
        .map(|rgb| Lab::from_rgb(rgb.color))
        // .inspect(|lab| println!("{:?}", lab))
        // .fold(|| Vec::new(), |mut acc, px| { acc.push(px); acc })
        // .flatten()
        .collect(),
    }
  }

  pub fn from_raw(width: usize, height: usize, data: Vec<Lab>) -> Self {
    Self { width, height, data }
  }

  pub fn dimensions(&self) -> (usize, usize) {
    (self.width, self.height)
  }

  pub fn get_pixel(&self, x: usize, y: usize) -> &Lab {
    &self.data[y * self.width + x]
  }
}

pub fn load_image(path: &str) -> LabImage {
  let img = image::open(path)
      .map(|result_img| result_img.to_rgb())
      .expect(&format!("could not open {} or it failed to parse", path));
  LabImage::from_srgb(&img)
}

pub fn load_images(image_paths: &[&str]) -> Vec<LabImage> {
  let input_images: Vec<_> = image_paths.iter()
    .cloned()
    .map(load_image)
    .collect();

  if !input_images[1..].iter().map(|img| img.dimensions()).all(
    |dim| {
      dim == input_images[0].dimensions()
    },
  )
  {
    eprintln!("warning: images are not the same size");
  }

  input_images
}

pub fn train_narrow_autoencoder(input_images: &[&LabImage], aperture_width: usize, neurons: usize, config_path: &str) -> nn::Network {
  let aperture = 0..(2*aperture_width as isize);
  let kernel = iproduct!(aperture.clone(), aperture).collect::<Vec<_>>();
  let kernel_ref = &kernel[..];
  let (out_width, out_height) = (
    input_images[0].width - aperture_width * 2,
    input_images[0].height - aperture_width * 2,
  );

  let mut rng: rand::XorShiftRng = rand::XorShiftRng::from_seed(rand::random());

  let mut net = {
    let ap = aperture_width;
    let mut net = nn::Network::from_definition(&nn::NetworkDefn {
      layers: vec![(2 * ap) * (2 * ap) * 3, neurons, (2 * ap) * (2 * ap) * 3],
      activation_coeffs: vec![2.0, 2.0],
      activation_fn: "sigmoid".into(),
    });
    net.assign_random_weights(&mut rng);
    net
  };
  let conf = {
    use std::fs::File;
    match File::open(config_path) {
      Ok(file) => sj::from_reader(file).unwrap(),
      Err(_) => panic!("no config file"),
    }
  };

  if input_images.len() < 2 {
    error!("Cannot proceed - need at least 2 images.");
  }

  let x_distribution = rand::distributions::Range::new(0, out_width);
  let y_distribution = rand::distributions::Range::new(0, out_height);
  const PATCHES_PER_IMAGE_PER_BATCH: usize = 1000;
  net.train_autoencoder(|| Some(
    {
      let mut result = Vec::with_capacity(input_images.len() * PATCHES_PER_IMAGE_PER_BATCH);
      for img in 0..input_images.len() {
        for _ in 0..PATCHES_PER_IMAGE_PER_BATCH {
          let patch = {
            let (x, y) = (x_distribution.ind_sample(&mut rng), y_distribution.ind_sample(&mut rng));
            kernel_ref.iter().flat_map(move |&(dx, dy)| {
              let lab = input_images[img].get_pixel(((x as isize) + dx) as usize, ((y as isize) + dy) as usize);
              vec![lab.l, lab.a, lab.b]
            }).collect::<Vec<_>>()
          };
          result.push(patch);
        }
      }
      result
    }
  ), None, &conf, None);

  net
}

pub fn features_from_image(net: &nn::Network, img: &LabImage, aperture_width: usize) -> Vec<(u32, u32, Vec<f32>)> {
  let aperture = 0..(2*aperture_width as isize);
  let kernel = iproduct!(aperture.clone(), aperture).collect::<Vec<_>>();
  let kernel_ref = &kernel[..];
  let (out_width, out_height) = (
    img.width - aperture_width * 2 + 1,
    img.height - aperture_width * 2 + 1,
  );

  (0..out_height)
    .into_par_iter()
    .map(move |y| {
      (0..out_width)
        .map(move |x| {
          let patch: Vec<_> = kernel_ref.iter().flat_map(move |&(dx, dy)| {
            let lab = img.get_pixel(
              x + dx as usize,
              y + dy as usize,
            );
            vec![lab.l, lab.a, lab.b]
          }).collect();

          let feature_vector = net.eval_to_layer(patch, 2);
          // println!("{} features", feature_vector.len());
          (x as u32, y as u32, feature_vector)
        })
        .collect::<Vec<_>>()
    })
    .fold(|| Vec::with_capacity(out_width), |mut acc, row| {
      acc.extend(row);
      acc
    })
    .flat_map(|chunk| chunk)
    .collect::<Vec<_>>()
}

pub fn raw_features_from_image(net: &nn::Network, img: &LabImage, aperture_width: usize) -> Vec<f32> {
  let aperture = 0..(2*aperture_width as isize);
  let kernel = iproduct!(aperture.clone(), aperture).collect::<Vec<_>>();
  let kernel_ref = &kernel[..];
  let (out_width, out_height) = (
    img.width - aperture_width * 2 + 1,
    img.height - aperture_width * 2 + 1,
  );

  (0..out_height)
    .into_par_iter()
    .flat_map(move |y| {
      (0..out_width)
        .map(move |x| {
          let patch: Vec<_> = kernel_ref.iter().flat_map(move |&(dx, dy)| {
            let lab = img.get_pixel(
              x + dx as usize,
              y + dy as usize,
            );
            vec![lab.l, lab.a, lab.b]
          }).collect();

          let feature_vector = net.eval_to_layer(patch, 2);
          // println!("{} features", feature_vector.len());
          feature_vector
        })
        .collect::<Vec<_>>()
    })
    .fold(|| Vec::with_capacity(out_width), |mut acc, row| {
      acc.extend(row);
      acc
    })
    .flat_map(|chunk| chunk)
    .collect::<Vec<_>>()
}

pub fn features_from_images(net: &nn::Network, imgs: &[&LabImage], aperture_width: usize) -> Vec<Vec<(u32, u32, Vec<f32>)>> {
  imgs.iter().map(|img| features_from_image(net, img, aperture_width)).collect()
}
pub fn raw_features_from_images(net: &nn::Network, imgs: &[&LabImage], aperture_width: usize) -> Vec<f32> {
  imgs.iter().flat_map(|img| raw_features_from_image(net, img, aperture_width)).collect()
}
