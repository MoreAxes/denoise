use ::*;

#[derive(Debug, Clone, Copy)]
pub struct DimScale {
  pub x: f32,
  pub y: f32,
  pub z: f32,
}

impl FromStr for DimScale {
  type Err = clap::Error;

  fn from_str(s: &str) -> clap::Result<Self> {
    let mut parts = s.split(",");
    let x = parts.next().unwrap_or("1.0").parse().unwrap_or(1.0);
    let y = parts.next().unwrap_or("1.0").parse().unwrap_or(1.0);
    let z = parts.next().unwrap_or("1.0").parse().unwrap_or(1.0);
    Ok(DimScale { x, y, z })
  }
}

impl Default for DimScale {
  fn default() -> Self {
    DimScale {
      x: 1.0,
      y: 1.0,
      z: 1.0,
    }
  }
}

pub fn median<I>(iter: I) -> Option<f32>
where
  I: IntoIterator<Item = f32>,
{
  use std::cmp::Ordering;

  let mut sorted = iter.into_iter().collect::<Vec<f32>>();
  sorted.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal));
  match sorted.len() {
    0 => None,
    1 => Some(sorted[0]),
    len => {
      if len % 2 == 0 {
        let (a, b) = (sorted[len / 2], sorted[len / 2 + 1]);
        Some((a + b) / 2.0)
      } else {
        Some(sorted[len / 2])
      }
    }
  }
}

pub fn squared_euclidean_distance(a: &(f32, f32, f32), b: &(f32, f32, f32), sc: DimScale) -> f32 {
  (a.0 - b.0) * (a.0 - b.0) * sc.x * sc.x + (a.1 - b.1) * (a.1 - b.1) * sc.y * sc.y +
    (a.2 - b.2) * (a.2 - b.2) * sc.z * sc.z
}

pub fn euclidean_distance(a: &(f32, f32, f32), b: &(f32, f32, f32), sc: DimScale) -> f32 {
  squared_euclidean_distance(a, b, sc).sqrt()
}

pub fn angular_distance(&(ax, ay, az): &(f32, f32, f32), &(bx, by, bz): &(f32, f32, f32), sc: DimScale) -> f32 {
  let dot = ax * bx * sc.x * sc.x + ay * by * sc.y * sc.y + az * bz * sc.z * sc.z;
  let a_magnitude_sq = ax * ax * sc.x * sc.x + ay * ay * sc.y * sc.y + az * az * sc.z * sc.z;
  let b_magnitude_sq = bx * bx * sc.x * sc.x + by * by * sc.y * sc.y + bz * bz * sc.z * sc.z;
  (dot / (a_magnitude_sq * b_magnitude_sq).sqrt()).acos()
}

pub fn least_vector_by_sum_impl<I, F>(candidates: &[I], observations: &[I], diff: F, sc: DimScale) -> Option<I>
where
  I: PartialEq + Clone,
  F: Fn(&I, &I, DimScale) -> f32,
{
  candidates
    .iter()
    .min_by(|&a, &b| {
      use std::cmp::Ordering;

      let arr = [a, b];
      let mut dist_sums = arr.iter().map(|el| {
        observations
          .iter()
          .filter_map(|other| if el != &other {
            Some(diff(el, other, sc))
          } else {
            None
          })
          .sum::<f32>()
      });
      let sum_a = dist_sums.next().unwrap();
      let sum_b = dist_sums.next().unwrap();
      sum_a.partial_cmp(&sum_b).unwrap_or(Ordering::Equal)
    })
    .cloned()
}

pub fn least_vector_by_sum<I, F>(iter: I, diff: F, sc: DimScale) -> Option<<I as IntoIterator>::Item>
where
  I: IntoIterator,
  <I as IntoIterator>::Item: PartialEq + Clone,
  F: Fn(&<I as IntoIterator>::Item,
     &<I as IntoIterator>::Item,
     DimScale) -> f32,
{
  let all = iter.into_iter().collect::<Vec<_>>();
  least_vector_by_sum_impl(&all, &all, diff, sc)
}

pub fn mean_of_least_by_sum<I, F>(count: usize, iter: I, diff: F, sc: DimScale) -> Option<(f32, f32, f32)>
where
  I: IntoIterator<Item = (f32, f32, f32)>,
  F: Fn(&(f32, f32, f32), &(f32, f32, f32), DimScale) -> f32,
{
  let samples = iter.into_iter().collect::<Vec<_>>();
  let mut permutation = (0..samples.len()).collect::<Vec<_>>();
  permutation.sort_unstable_by(|&a, &b| {
    use std::cmp::Ordering;

    let arr = [a, b];
    let mut dist_sums = arr.iter().map(|&el| {
      samples
        .iter()
        .filter_map(|other| if &samples[el] != other {
          Some(diff(&samples[el], other, sc))
        } else {
          None
        })
        .sum::<f32>()
    });
    let sum_a = dist_sums.next().unwrap();
    let sum_b = dist_sums.next().unwrap();
    sum_a.partial_cmp(&sum_b).unwrap_or(Ordering::Equal)
  });
  Some(permutation.into_iter().take(count).fold(
    (0.0, 0.0, 0.0),
    |acc, it| {
      (
        acc.0 + samples[it].0 / count as f32,
        acc.1 + samples[it].1 / count as f32,
        acc.2 + samples[it].2 / count as f32,
      )
    },
  ))
}

pub fn least_vector_by_sum_optimized<I, F, R>(
  iter: I,
  diff: F,
  sc: DimScale,
  rng: &mut R,
) -> Option<<I as IntoIterator>::Item>
where
  I: IntoIterator<Item = (f32, f32, f32)>,
  F: Fn(&<I as IntoIterator>::Item,
     &<I as IntoIterator>::Item,
     DimScale) -> f32,
  R: rand::Rng,
{
  use rand::*;
  use rand::distributions::IndependentSample;

  let all = iter.into_iter().collect::<Vec<_>>();
  let best_sample = least_vector_by_sum_impl(&all, &all, &diff, sc)?;
  let gaussian = distributions::Normal::new(0.0, 0.33);
  let mut new_samples = Vec::with_capacity(20);
  new_samples.push(best_sample);
  new_samples.extend((1..20).map(|_| {
    (
      best_sample.0 + gaussian.ind_sample(rng) as f32,
      best_sample.1 + gaussian.ind_sample(rng) as f32,
      best_sample.2 + gaussian.ind_sample(rng) as f32,
    )
  }));
  least_vector_by_sum_impl(&new_samples, &all, diff, sc)
}

#[derive(EnumString, Debug, PartialEq, Eq)]
pub enum Operation {
  #[strum(serialize = "channel-mean")]
  ChannelMean,
  #[strum(serialize = "channel-median")]
  ChannelMedian,
  #[strum(serialize = "lowest-ranking")]
  LowestRanking,
}

#[derive(EnumString, Debug, PartialEq, Eq)]
pub enum RankFunction {
  #[strum(serialize = "euclidean")]
  Euclidean,
  #[strum(serialize = "squared-euclidean")]
  SquaredEuclidean,
  #[strum(serialize = "angular")]
  Angular,
}

pub fn apply_filter(labs: Vec<Lab>, image_pool_size: usize, opt: &Opt) -> Lab {
  use self::Operation::*;
  use self::RankFunction::*;

  let new_lab = match &opt.operation {
    &ChannelMean => {
      labs.iter().fold(
        Lab::new(0.0, 0.0, 0.0),
        |acc, lab| acc + *lab,
      ) / image_pool_size as f32
    }
    &ChannelMedian => {
      Lab::new(
        filters::median(labs.iter().map(|lab| lab.l)).unwrap_or(0.0),
        filters::median(labs.iter().map(|lab| lab.a)).unwrap_or(0.0),
        filters::median(labs.iter().map(|lab| lab.b)).unwrap_or(0.0),
      )
    }
    &LowestRanking => {
      let normalized_in = labs.iter().map(|lab| {
        (
          lab.l / 100.0,
          (lab.a + 128.0) / 255.0,
          (lab.b + 128.0) / 255.0,
        )
      });
      let rank_function = match &opt.rank_function {
        &Euclidean => filters::euclidean_distance,
        &SquaredEuclidean => filters::squared_euclidean_distance,
        &Angular => filters::angular_distance,
      };
      let (norm_l, norm_a, norm_b) = if !opt.optimize {
        if opt.mean_of_lowest == 0 {
          filters::least_vector_by_sum(
            normalized_in,
            rank_function,
            opt.dimension_scale,
          )
        } else {
          filters::mean_of_least_by_sum(
            opt.mean_of_lowest,
            normalized_in,
            rank_function,
            opt.dimension_scale,
          )
        }
      } else {
        use rand::*;
        let mut rng = XorShiftRng::from_seed(thread_rng().gen());
        filters::least_vector_by_sum_optimized(
          normalized_in,
          rank_function,
          opt.dimension_scale,
          &mut rng,
        )
      }.unwrap_or((0.0, 0.0, 0.0));
      Lab::new(
        norm_l * 100.0,
        norm_a * 255.0 - 128.0,
        norm_b * 255.0 - 128.0,
      )
    }
  };
  new_lab
}
