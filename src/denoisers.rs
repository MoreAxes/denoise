#![feature(pointer_methods)]

extern crate libc;

#[macro_use]
extern crate log;
extern crate fingers as nn;
extern crate image;
#[macro_use]
extern crate itertools;
#[macro_use]
extern crate palette;
extern crate rand;
extern crate rayon;
extern crate serde;
extern crate serde_json as sj;

use libc::*;

use std::ffi::*;

mod common;

use common::*;

#[no_mangle]
pub extern fn den_load_image(path: *const c_char, out_width: *mut size_t, out_height: *mut size_t) -> *const c_void {
  let img = load_image(unsafe { CStr::from_ptr(path) }.to_str().unwrap());
  unsafe {
    if !out_width.is_null() {
      *out_width = img.width;
    }
    if !out_height.is_null() {
      *out_height = img.height;
    }
  }
  let x = Box::into_raw(Box::new(img));
  eprintln!("den_load_image: {:?}", x);
  x as *const c_void
}

// #[no_mangle]
// pub extern fn den_load_images(paths: *const *const c_char, paths_count: size_t) -> *const c_void {
//   let vec = (0..paths_count)
//       .map(|p| den_load_image(unsafe { *paths.offset(p as isize) }))
//       .collect::<Vec<_>>();
//   Vec::into_raw(vec)
// }

#[no_mangle]
pub extern fn den_get_feature_vector_count(image: *const c_void, aperture: size_t) -> size_t {
  eprintln!("den_get_feature_vector_count: {:?}", image);
  let img = unsafe { &*(image as *const LabImage) };

  let (out_width, out_height) = (
    img.width - aperture * 2 + 1,
    img.height - aperture * 2 + 1,
  );

  out_width * out_height
}

#[no_mangle]
pub extern fn den_train_and_extract_features(images: *const *const c_void, images_count: size_t, aperture: size_t, features: size_t, out_ptr: *mut int8_t) -> size_t {
  let images_ptr_vec = (0..images_count)
      .map(|p| unsafe { *images.offset(p as isize) } as *const LabImage)
      .collect::<Vec<_>>();
  eprintln!("den_train_and_extract_features: {:?}, out_ptr: {:?}", images_ptr_vec, out_ptr);
  let images_vec = images_ptr_vec.into_iter()
      .map(|p| unsafe { &*p })
      .collect::<Vec<_>>();

  let net = train_narrow_autoencoder(&images_vec, aperture, features, "Config.json");
  let mut feat = raw_features_from_images(&net, &images_vec, aperture).into_iter().map(|x| ((x - 0.5) * 64.0) as i8).collect::<Vec<_>>();
  eprintln!("den_train_and_extract_features: {} features extracted in total", feat.len());
  // Box::into_raw(feat.into_boxed_slice())
  // feat.shrink_to_fit();
  let ptr = feat.as_mut_ptr();
  unsafe { ptr.copy_to(out_ptr, feat.len()) };
  // // let len = feat.len();
  // ::std::mem::forget(feat);
  // ptr

  // for &f in &feat {
  //   unsafe { *out_ptr = f };
  //   out_ptr = out_ptr.offset(1);
  // }
  eprintln!("den_train_and_extract_features: leaving");
  feat.len()
}
