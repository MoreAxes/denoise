Aby zbudować projekt, należy postępować zgodnie z następującymi krokami:

1. Zawartości archiwów nalgebra.zip, finge-rs.zip oraz denoise.zip wypakować do
  tego samego katalogu (zachować główny podfolder).
2. Zainstalować platformę Rust zgodnie z instrukcją na stronie
  https://www.rust-lang.org/en-US/install.html. Po prawidłowej instalacji w
  wierszu poleceń powinno być dostępne narzędzie `cargo`. Wersja kompilatora
  używana przez autora to `rustc 1.22.0-nightly (4279e2b4c 2017-10-21)`
3. W wypakowanym katalogu `denoise` wywołać `cargo build --release`. Po zakończonej
  kompilacji w podkatalogu `denoise/target/release` powinien znaleźć się plik denoisers.lib.
4. W `denoise/cpp/CMakeLists.txt` prawidłowo ustawić ścieżkę do w/w pliku w liniach 16 i 17.
5. Za pomocą cmake-gui skonfigurować część C++ projektu. (Wymaga to
  skompilowania OpenCV za pomocą pasującego kompilatora. Autor używał MinGW 6.2.0.
  Gotowe paczki MinGW znajdują się na https://nuwen.net/mingw.html). Podczas
  generowania projektu w cmake-gui należy wybrać MinGW Makefiles oraz dodać
  "-std=gnu++14 -fopenmp" do CMAKE_CXX_FLAGS.
6. Po wygenerowaniu projektu w cmake-gui należy przejść do katalogu, który
  wybraliśmy jako docelowy w cmake-gui, i wykonać mingw32-make.

Aby programy d-noise i d-noise-ss mogły zadziałać prawidłowo, muszą być wykonywane
z katalogu, w którym znajduje się plik `denoise/Network.json`. Zawiera on konfigurację
algorytmu uczącego autoenkoder.
